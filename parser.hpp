#ifndef PARSER_HPP_INCLUDED
#define PARSER_HPP_INCLUDED
#include "graph.hpp"
#include <vector>
#include <string>

int parseTextCount(std::string const& text);

void parseTextEdges(const std::string& text, std::vector<Node>& nodes);

#endif // PARSER_HPP_INCLUDED
