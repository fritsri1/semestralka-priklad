#include "graph.hpp"
#include <vector>
#include <string>
#include <sstream>

//Converts text to int and ensures it is at least 1
int parseTextCount(std::string const& text) {
    try {
        int nodes = std::stoi(text);
        if (nodes < 1) throw std::invalid_argument("At least 1 node is required");
        return nodes;
    } catch (const std::exception& e) {
        throw std::invalid_argument("Couldn't convert first line to int");
    }
}

void parseTextEdges(const std::string& text, std::vector<Node>& nodes) {
    std::istringstream strStream(text);
    std::string temp;
    std::vector<std::string> edgeValues;

    //Split string by commas
    while(getline(strStream, temp, ',')) {
        edgeValues.push_back(temp);
    }
    if (edgeValues.size() % 3 != 0) {
        throw std::invalid_argument("Edges must be in the format \"from,to,weight\", separated by commas!");
    }

    //Take 3 elements and attempt to create edge
    for (int i = 0; i < edgeValues.size(); i += 3) {
        try {
            int from = std::stoi(edgeValues[i]);
            int to = std::stoi(edgeValues[i + 1]);
            int weight = std::stoi(edgeValues[i + 2]);
            nodes.at(from).addEdge(to, weight);
        } catch (const std::exception& e) {
            throw std::invalid_argument("Couldn't convert an edge argument to int");
        }
    }
}
