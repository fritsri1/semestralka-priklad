#ifndef GENERATOR_HPP_INCLUDED
#define GENERATOR_HPP_INCLUDED
#include "graph.hpp"
#include <vector>

void generateRandomGraph(int nodeCount, std::vector<Node>& nodes);

#endif // GENERATOR_HPP_INCLUDED
