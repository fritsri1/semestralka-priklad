#ifndef SOLVER_HPP_INCLUDED
#define SOLVER_HPP_INCLUDED
#include "graph.hpp"
#include <vector>
#include <iostream>

class Distance {
private:
    int value;
    bool infinite;

public:
    Distance() : value(0), infinite(true) {}
    Distance(int value) : value(value), infinite(false) {}

    bool isInfinite() const;
    int getValue() const;

    void setValue(int value);
    void setToInfinite();

    bool operator==(const Distance& rhs) const;
    bool operator<(const Distance& rhs) const;
    bool operator>(const Distance& rhs) const;
    bool operator<=(const Distance& rhs) const;
    bool operator>=(const Distance& rhs) const;
    Distance operator+(const Distance& rhs) const;

};

class Solver {
private:
    int nodeCount = 0;
    std::vector<std::vector<Distance>> distances;
    std::vector<Node> nodes;

public:
    Solver(int nodeCount) : nodeCount(nodeCount) {
        for (int i = 0; i < nodeCount; i++) {
            distances.push_back(std::vector<Distance>());
            for (int j = 0; j < nodeCount; j++) {
                distances.at(i).push_back(Distance());
            }
        }
    }

    Solver(int nodeCount, std::vector<Node> nodes) : nodeCount(nodeCount), nodes(nodes) {
        for (int i = 0; i < nodeCount; i++) {
            distances.push_back(std::vector<Distance>());
            for (int j = 0; j < nodeCount; j++) {
                distances.at(i).push_back(Distance());
            }
        }
    }

    std::vector<std::vector<Distance>> getDistances() const;
    void setNodes(const std::vector<Node>& nodes);

    void solveSt();
    void solveMt(int threadCount);
    void reset();

    void print(std::ostream& out) const;
};

#endif // SOLVER_HPP_INCLUDED
