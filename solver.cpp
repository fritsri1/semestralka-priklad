#include "solver.hpp"

#include <algorithm>

struct nodesNdDists {
    std::vector<std::vector<Distance>>& distances;
};

bool Distance::isInfinite() const {
    return infinite;
}

int Distance::getValue() const {
    return value;
}

void Distance::setValue(int value) {
    if (infinite) infinite = false;
    this->value = value;
}

void Distance::setToInfinite() {
    infinite = true;
}

bool Distance::operator==(const Distance& rhs) const {
    if (infinite != rhs.infinite) return false;
    else return infinite && rhs.infinite ? true : value == rhs.value;
}

bool Distance::operator<(const Distance& rhs) const {
    if (infinite) return false;
    else return rhs.infinite ? true : value < rhs.value;
}

bool Distance::operator>(const Distance& rhs) const {
    return rhs < *this;
}

bool Distance::operator<=(const Distance& rhs) const {
    return !(*this > rhs);
}

bool Distance::operator>=(const Distance& rhs) const {
    return !(*this < rhs);
}

Distance Distance::operator+(const Distance& rhs) const {
    return infinite || rhs.infinite ? Distance() : Distance(value + rhs.value);
}

std::ostream& operator<<(std::ostream& os, const Distance& dist) {
    if (dist.isInfinite()) os << "inf";
    else os << dist.getValue();
    return os;
}

void Solver::setNodes(const std::vector<Node>& nodes) {
    this->nodes = nodes;
}

std::vector<std::vector<Distance>> Solver::getDistances() const {
    return distances;
}

void Solver::solveSt() {
    for (const Node& node : nodes) {
        for (const std::pair<const int, int>& edge : node.getEdges()) {
            distances[node.getNumber()][edge.first].setValue(edge.second);
        }
    }
    for (int i = 0; i < nodeCount; i++) {
        distances[i][i].setValue(0);
    }
    for (int mid = 0; mid < nodeCount; mid++) {
        for (int start = 0; start < nodeCount; start++) {
            for (int dest = 0; dest < nodeCount; dest++) {
                Distance newPath = distances[start][mid] + distances[mid][dest];
                if (start == dest && newPath < 0) throw std::logic_error("The graph can't have a negative loop");
                if (distances[start][dest] > newPath) distances[start][dest] = newPath;
            }
        }
    }
}

void Solver::solveMt(int threadCount) {

}

void Solver::reset() {
    for (int i = 0; i < nodeCount; i++) {
        for (int j = 0; j < nodeCount; j++) {
            distances[i][j].setToInfinite();
        }
    }
}

void Solver::print(std::ostream& out) const {
    out << "Distance matrix:" << std::endl
        << "From---weight--->To" << std::endl;
    for (int i = 0; i < nodeCount; i++) {
        for (int j = 0; j < nodeCount; j++) {
            out << i << "---" << distances[i][j] << "--->" << j << std::endl;
        }
    }
}
