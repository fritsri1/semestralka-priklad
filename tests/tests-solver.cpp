#include "../generator.hpp"
#include "../solver.hpp"
#include "catch.hpp"

#include <iostream>
#include <vector>

TEST_CASE("Distance class tests") {
    Distance inf1;
    Distance inf2;
    Distance bigger(10);
    Distance smaller(-50);

    REQUIRE(!(inf1 < inf2));
    REQUIRE(!(inf1 > inf2));
    REQUIRE(inf1 >= inf2);
    REQUIRE(inf1 <= inf2);
    REQUIRE(bigger < inf1);
    REQUIRE(smaller < inf1);
}

TEST_CASE("Basic graph solution") {
    Node one(0);
    Node two(1);
    Node three(2);
    one.addEdge(three.getNumber(), 12);
    one.addEdge(two.getNumber(), -4);
    two.addEdge(three.getNumber(), 5);
    std::vector<Node> nodes;
    nodes.push_back(one);
    nodes.push_back(two);
    nodes.push_back(three);
    Solver solver(3);
    solver.setNodes(nodes);
    solver.solveSt();
    REQUIRE(solver.getDistances().at(0).at(1).getValue() == -4);
    REQUIRE(solver.getDistances().at(1).at(2).getValue() == 5);
    REQUIRE(solver.getDistances().at(0).at(2).getValue() == 1);
}

//TEST_CASE("Multi-thread solution equals single-thread") {
//    std::vector<Node> graph;
//    generateRandomGraph(20, graph);
//    Solver solver(20);
//    solver.setNodes(graph);
//    solver.solveSt();
//    std::vector<std::vector<Distance>> resultSt = solver.getDistances();
//    solver.reset();
//    solver.solveMt(4);
//    std::vector<std::vector<Distance>> resultMt = solver.getDistances();
//    REQUIRE(resultSt == resultMt);
//}
