#include "../graph.hpp"
#include "catch.hpp"

TEST_CASE("Node and edge creation") {
    Node one(1);
    Node two(2);
    one.addEdge(two.getNumber(), 3);
    REQUIRE(one.getEdges().at(two.getNumber()) == 3);
}


TEST_CASE("Change Number of existing edge") {
    Node one(1);
    Node two(2);
    one.addEdge(two.getNumber(), 3);
    one.addEdge(two.getNumber(), -20);
    REQUIRE(one.getEdges().at(two.getNumber()) == -20);
}
