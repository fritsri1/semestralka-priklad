#include "graph.hpp"
#include <cmath>
#include <set>
#include <vector>

void generateRandomGraph(int nodeCount, std::vector<Node>& nodes) {
    int edgeCount = nodeCount*(nodeCount - 1) / 2;
    std::set<std::pair<int, int>> edges;

    for (int i = 0; i < nodeCount; i++) {
        Node node(i);
        nodes.push_back(node);
    }
    for (int i = 0; i < edgeCount; i++) {
        int a = rand() % nodeCount;
        int b = rand() % nodeCount;
        std::pair<int, int> p = std::make_pair(a, b);

        while (edges.find(p) != edges.end()) {
            a = rand() % nodeCount;
            b = rand() % nodeCount;
            p = std::make_pair(a, b);
        }
        edges.insert(p);
        nodes.at(a).addEdge(b, 1 + rand() % 20);
    }
}
