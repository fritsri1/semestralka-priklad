#ifndef GRAPH_HPP_INCLUDED
#define GRAPH_HPP_INCLUDED
#include <map>

class Node {
private:
    int number;
    std::map<int, int> edges;

public:
    Node(int number) : number(number) {}

    int getNumber() const;
    const std::map<int, int>& getEdges() const;
    void addEdge(int, int weight);
};

#endif // GRAPH_HPP_INCLUDED
