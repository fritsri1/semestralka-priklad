#include "graph.hpp"
#include <iostream>
#include <algorithm>

int Node::getNumber() const {
    return number;
}

const std::map<int, int>& Node::getEdges() const {
    return edges;
}

void Node::addEdge(int destination, int weight) {
    if (std::any_of(edges.cbegin(), edges.cend(), [destination, weight](const auto& edge) {
    return edge.first == destination;
})) {
        std::clog << "Overwriting existing edge!" << std::endl;
    }
    if (number == destination && weight < 0) std::cerr << "Can't create negative edge from a node to itself!" << std::endl;
    else edges[destination] = weight;
}
