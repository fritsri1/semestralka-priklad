# Zadání
Implementace algoritmu Floyd-Warshall pro nalezení všech nejkratších
cest mezi vrcholy v orientovaném, ohodnoceném grafu.

# Implementace
Aplikace při spuštění bez specifikace cesty k souboru sama vyřeší
náhodně vygenerovaný graf o 15 vrcholech. Při specifikaci souboru
se přečtou data z daného souboru a vyřeší se.

## Formát grafu při čtení ze souboru
Na prvním řádku je napsán počet vrcholů. Na druhém řádku se za sebou
zapisují hrany ve formátu "od,do,váha". Samotné hrany se od sebe
oddělují rovněž čárkami. Pro příklad se lze podívat na soubor
"input" umístěný v repozitáři.

## Vícevláknová implementace
Vícevláknovou implementaci jsem v semestru nestihl zpracovat.
Pro tento konkrétní algoritmus je náročnější, většina volně
dostupných implementací využívala komplikovanější algoritmy
či výpočet pomocí GPU.

Moje vlastní naivní implementace celou aplikaci pouze výrazně
zpomalovala, rozhodl jsem se ji proto raději vynechat.

