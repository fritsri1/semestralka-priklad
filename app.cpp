#include "parser.hpp"
#include "solver.hpp"
#include "generator.hpp"
#include <fstream>
#include <iostream>

// Solves a randomly generated graph
void solveRandomGraph() {
    int nodeCount = 15;
    Solver solver(nodeCount);
    std::vector<Node> nodes;

    generateRandomGraph(nodeCount, nodes);
    solver.setNodes(nodes);
    try {
        solver.solveSt();
    } catch (const std::exception& e) {
        std::cerr << e.what();
    }
    solver.print(std::cout);
}

//Parses first and second line of file.
//Than it solves the parsed graph.
void solveFileGraph(char* filename) {
    int nodeCount;
    std::string textCount;
    std::string textEdges;
    std::ifstream inputFile;
    std::vector<Node> nodesCount;

    inputFile.open(filename);
    if (!inputFile.is_open()) {
        std::cerr << "File doesn't exist or couldn't be opened" << std::endl;
        return;
    }
    getline(inputFile, textCount);
    getline(inputFile, textEdges);
    try {
        nodeCount = parseTextCount(textCount);
        for (int i = 0; i < nodeCount; i++) {
            nodesCount.push_back(Node(i));
        }
        parseTextEdges(textEdges, nodesCount);
        Solver solver(nodeCount, nodesCount);
        solver.solveSt();
        solver.print(std::cout);
    } catch (const std::exception& e) {
        std::cerr << e.what();
    }
}
