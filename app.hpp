#ifndef APP_HPP_INCLUDED
#define APP_HPP_INCLUDED

void solveRandomGraph();

void solveFileGraph(char* filename);

#endif // APP_HPP_INCLUDED
