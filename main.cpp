#define _USE_MATH_DEFINES
#include "app.hpp"

#include <iostream>
#include <algorithm>
#include <string>

void print_usage(std::string const& exe_name) {
    std::cout << "Usage: "<< exe_name << "[OPTION] [FILE]" << std::endl
              << "or: fw-path [OPTION]" << std::endl
              << "-h, --help:    prints this help" << std::endl;
}

bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

bool is_argument(std::string const& argument) {
    return argument.find("-") != std::string::npos || argument.find("--") != std::string::npos;
}

int main(int argc, char** argv) {
    if (std::any_of(argv, argv+argc, is_help)) {
        print_usage(argv[0]);
        return 0;
    }
    if (std::any_of(argv + 1, argv+argc, is_argument)) {
        std::cerr << "Invalid argument, printing help:" << std::endl;
        print_usage(argv[0]);
        return 0;
    }
    if (argc == 1) {
        solveRandomGraph();
    } else {
        solveFileGraph(argv[1]);
    }
    return 0;
}
